# Techno Web Serveur - TP1

## Question 1

Les fonctionnalités importantes:

1. Chaque attribution de gommette doit être accompagnée d'un **commentaire** décrivant l'action pour laquelle la gommette a été attribuée.
2. Les parents doivent pouvoir **consulter** les gommettes de leur enfant, et peut-être signer le carnet par l'application pour attester qu'ils ont pris connaissance des différentes gommettes.
3. L'application pourrait également intégrer une **messagerie** entre les parents et les professeurs qui leur permettrait d'échanger à propos des gommettes.

## Question 2

Les classes Java:

- Gommette : Elle doit etre accompagnée d'un commentaire. Elle peut être de trois couleurs différentes:
  - **Blanche**
  - <span style = "color : green"> **Verte** </span>
  - <span style = "color : red"> **Rouge** </span>
- Carnet : Il est associé à un enfant (par son nom) et contient un nombre indéfini de gommettes.
- Personne : trois classes en héritent:
  - Parent : Il doit avoir accès à un ou plusieurs carnets, si la personne a plusieurs enfants.
  - Professeur : Il doit pouvoir ajouter des gommettes au carnet.

## Question 3

Les tables de la base de donnée seront:

- Professeur
  - username
  - password
- Parent
  - username
  - password
  - carnet : references Carnet
- Carnet
  - nom_enfant : varchar()
  - gommettes : Gommettes[]
- Gommette
  - couleur : varchar(1)
  - commentaire : text

## Question 4

Les solutions envisagées:

1. Une bdd sqlite
2. un backend Java
