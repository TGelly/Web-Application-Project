DROP TABLE IF EXISTS teacher;
CREATE TABLE teacher
(
    idTeacher int;
    nameTeacher varchar();

);

DROP TABLE IF EXISTS sticker;
CREATE TABLE sticker
(
    idSticker int;
    dateGommette date;
    couleur varchar(10);
    commentaire varchar();

);

DROP TABLE IF EXISTS book;
CREATE TABLE book
(
    idBook int;
    nameChild varchar();

    PRIMARY key (idBook);
);

DROP TABLE IF EXISTS parent;
CREATE TABLE parent
(
    idParent int;
    childsBook int;
    PRIMARY KEY (idParent);
    FOREIGN KEY (childsBook) REFERENCES book(idBook);

);

insert into teacher values();

-- la suite est en travaux

DROP TABLE IF EXISTS competences;
CREATE TABLE competences
(
    idCompetences INTEGER,
    nom VARCHAR(200),
    magie INTEGER,
    dommages INTEGER,
    PRIMARY KEY (idCompetences)
);



.mode csv
.separator ','

.import /home/thibault/Documents/Info/VSCode/L2/S4/Databases/TP4/personnages.csv personnages
.import /home/thibault/Documents/Info/VSCode/L2/S4/Databases/TP4/classes.csv classes
.import /home/thibault/Documents/Info/VSCode/L2/S4/Databases/TP4/competences.csv competences

.separator '|'
