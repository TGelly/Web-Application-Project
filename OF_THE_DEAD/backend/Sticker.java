package OF_THE_DEAD.backend;

public class Sticker {

    private String color;       // Red, Green or White
    private String teacher;     // Name of the teacher that put this sticker
    private String comment;     // comment written by the teacher

    public Sticker(String initColor, String initComment){
        this.color = initColor;
        this.comment = initComment;
    }
    
}
